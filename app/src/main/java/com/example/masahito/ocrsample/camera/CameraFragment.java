package com.example.masahito.ocrsample.camera;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.example.masahito.ocrsample.R;

import java.io.IOException;

/**
 * カメラフラグメント
 */
public class CameraFragment extends Fragment implements SurfaceHolder.Callback {
    // クラス名
    final static private String CLASS_NAME = "CameraFragment";

    // カメラマネージャー
    private CameraManager cameraManager;

    // サーフェス保持フラグ
    private boolean hasSurface = false;

    // サーフェスビューインスタンス
    private SurfaceView surfaceView;

    /**
     * コンストラクタ
     */
    public CameraFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(CLASS_NAME, "onCreateView");

        // ビューの初期化
        View view = inflater.inflate(R.layout.fragment_caputure, container, false);
        surfaceView = (SurfaceView) view.findViewById(R.id.preview_view);

        // サーフェスホルダーの設定
        SurfaceHolder holder = surfaceView.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        return view;
    }

    @Override
    public void onResume() {
        Log.d(CLASS_NAME, "onResume");

        // 初期化
        super.onResume();

        // カメラマネージャーのインスタンス生成
        cameraManager = new CameraManager(getActivity().getApplication());
    }

    @Override
    public void onPause() {
        Log.d(CLASS_NAME, "onPause hasSurface:" + hasSurface);

        // カメラ停止
        cameraManager.closeDriver();
        if (!hasSurface) {
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }

        // 停止処理
        super.onPause();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(CLASS_NAME, "surfaceCreated hasSurface:" + hasSurface);

        // カメラ初期化呼び出し
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder);
        }

        // カメラプレビュー開始
        cameraManager.startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(CLASS_NAME, "surfaceChanged");

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(CLASS_NAME, "surfaceDestroyed");

        // サーフェス保持フラグをOFF
        hasSurface = false;
    }

    /**
     * カメラ初期化
     *
     * @param surfaceHolder サーフェスホルダーインスタンス
     */
    private void initCamera(SurfaceHolder surfaceHolder) {
        Log.d(CLASS_NAME, "initCamera");

        // 引数チェック
        if (surfaceHolder == null) {
            Log.d(CLASS_NAME, "surfaceHolder is null");

            // nullの場合はエラーをthrow
            throw new IllegalStateException("サーフェスホルダーインスタンスが設定されていません");
        }

        // 初期化済みチェック
        if (cameraManager.isOpen()) {
            Log.w(CLASS_NAME, "カメラ初期化は既に実行済みです");
            return;
        }

        // カメラの起動
        try {
            cameraManager.openDriver(surfaceHolder);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            Log.w(CLASS_NAME, ioe);
        } catch (RuntimeException e) {
            e.printStackTrace();
            Log.w(CLASS_NAME, "想定しないエラーが発生しました", e);
        }
    }
}
