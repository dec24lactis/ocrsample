package com.example.masahito.ocrsample.activity;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.example.masahito.ocrsample.R;
import com.example.masahito.ocrsample.camera.CameraFragment;

public class CaputureActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 初期化
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caputure);

        // フルスクリーン化
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // フラグメントの設定
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.fragment, new CameraFragment()).commit();
        }
    }
}
